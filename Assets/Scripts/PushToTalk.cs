﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Voice.PUN;
using Photon.Voice.Unity;

public class PushToTalk : MonoBehaviourPun
{
    public KeyCode talkButton = KeyCode.Space;
    public Recorder voiceRecorder;


    void Start()
    {
        voiceRecorder.TransmitEnabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(talkButton))
        {
            voiceRecorder.TransmitEnabled = true;
        }
        else if (Input.GetKeyUp(talkButton))
        {
            voiceRecorder.TransmitEnabled = false;

        }
    }
}
