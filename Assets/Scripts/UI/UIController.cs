﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UIController : IUIController
{

    public IUIView UIView => _uIView;
    public IPhotonController PhotonController => _photonController;


    [Inject] private IUIView _uIView; 
    [Inject] private IPhotonController _photonController;


    public void Disconnect()
    {
        _photonController.Disconnect();

    }

    public void TryToJoinRoom()
    {
        _photonController.TryToJoinRoom();
    }
}
