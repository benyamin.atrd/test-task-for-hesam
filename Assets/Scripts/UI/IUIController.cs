﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIController
{
    IUIView UIView { get; }
    IPhotonController PhotonController { get; }

    void TryToJoinRoom();
    void Disconnect();
}
