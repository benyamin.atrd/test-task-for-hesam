﻿
public interface IUIView 
{
    IUIController IUIController{ get; }

    void OnJointRoom();
    void OnConnectingRoom();
    void OnLeaveRoom();
}
