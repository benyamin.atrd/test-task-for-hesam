﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


public class UIView  : MonoBehaviour, IUIView
{
    public IUIController IUIController => _iUIController;

    [SerializeField] GameObject _connectingPanel;
    [SerializeField] Button _joinB;
    [SerializeField] Button _leaveB;

    [Inject]
    private IUIController _iUIController;

    public void OnConnectingRoom()
    {
        _joinB.gameObject.SetActive(false);
        _leaveB.gameObject.SetActive(false);
        _connectingPanel.gameObject.SetActive(true);
    }
    public void OnJointRoom()
    {
        _joinB.gameObject.SetActive(false);
        _leaveB.gameObject.SetActive(true);
        _connectingPanel.gameObject.SetActive(false);
    }
    public void OnLeaveRoom()
    {
        _joinB.gameObject.SetActive(true);
        _leaveB.gameObject.SetActive(false);
    }


    private void Start()
    {
        _joinB.onClick.AddListener(IUIController.TryToJoinRoom);
        _leaveB.onClick.AddListener(IUIController.Disconnect);
    }
}
