﻿using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Zenject;

public class PhotonController : MonoBehaviourPunCallbacks, IPhotonController
{

    public enum Status { Offline, Connecting, Joining, Creating, Rooming };

    public string room = "The Room";
    public string avatar = "Avatar";

    [Inject]
    private IUIController uIManager;

    [Header("Informative")]
    public Status status = Status.Offline;


    public void TryToJoinRoom()
    {
        status = Status.Connecting;
        PhotonNetwork.ConnectUsingSettings();
        uIManager.UIView.OnConnectingRoom();

    }
    public void Disconnect()
    {
        status = Status.Offline;

        PhotonNetwork.Disconnect();
    }
    public override void OnConnectedToMaster()
    {
        status = Status.Joining;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        status = Status.Creating;
        PhotonNetwork.CreateRoom(room);
    }


    public override void OnJoinedRoom()
    {
        status = Status.Rooming;
        Log("Joined room");
        uIManager.UIView.OnJointRoom();

        if (!string.IsNullOrEmpty(avatar))
        {
            PhotonNetwork.Instantiate(avatar, Vector3.zero, Quaternion.identity, 0);
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        if (cause != DisconnectCause.DisconnectByClientLogic)
            Err("Photon error: " + cause);

        uIManager.UIView.OnLeaveRoom();

    }

    void Log(string x) { Debug.Log(x); }

    void Err(string x) { Debug.LogWarning(x); }


}
