﻿using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Zenject;

public interface IPhotonController  {

    void TryToJoinRoom();
    void Disconnect();

}
