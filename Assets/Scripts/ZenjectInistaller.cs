﻿using Zenject;

public class ZenjectInistaller : MonoInstaller
{

    public override void InstallBindings()
    {

        Container.Bind<IUIView>()
        .To<UIView>()
        .FromComponentInHierarchy()
        .AsSingle();

        Container.Bind<IPhotonController>()
        .To<PhotonController>()
        .FromComponentInHierarchy()
        .AsSingle();

        Container.Bind<IUIController>()
        .To<UIController>()
        .AsSingle();
    }
}
