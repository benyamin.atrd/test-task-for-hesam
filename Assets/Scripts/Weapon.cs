﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using UniRx.Triggers;
using UniRx;
using System;

public class Weapon : MonoBehaviour {

	public KeyCode key = KeyCode.Space;
	public GameObject beam;
	private float _doubleTapThrishold = .2f;

	private void Start()
	{
		var enableStream = this.UpdateAsObservable()
		.Where(x => Input.GetKeyDown(key));

		enableStream.Buffer(enableStream.Throttle(TimeSpan.FromSeconds(_doubleTapThrishold)))
		.Where(x => x.Count >= 2).Subscribe(x => beam.SetActive(true));

		enableStream.Buffer(enableStream.Throttle(TimeSpan.FromSeconds(_doubleTapThrishold)))
        .Where(x => x.Count == 1).Subscribe(x => beam.SetActive(false));
	}

}
